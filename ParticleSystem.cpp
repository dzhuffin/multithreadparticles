#include <thread>
#include <mutex>
#include "nvToolsExt.h"
#include "test.h"
#include "Particle.h"
#include "ParticlesPool.h"
#include "ParticleSystem.h"


//-------------------------------------------------------------------------------
using namespace test;


//-------------------------------------------------------------------------------
ParticleSystem::SimulationData::SimulationData(size_t numParticles, uint64_t randomSeed) :
	pool(new ParticlesPool(numParticles)), pAliveParticles(nullptr),
	effectRequest(EffectRequest(0.0f, 0.0f, false)), randomEngine(randomSeed) {}


//-------------------------------------------------------------------------------
ParticleSystem::SimulationData::~SimulationData() { delete pool; }


//-------------------------------------------------------------------------------
ParticleSystem::ParticleSystem() :
	stopSimulations(false)
{
	size_t maxThreadsCount = std::thread::hardware_concurrency();
	if (maxThreadsCount == 0) // if undefined use 1 thread
		maxThreadsCount = 1;

	int particlesPerThread = MAX_PARTICLES_COUNT / maxThreadsCount;
	int assignedParticlesCount = 0;
	for (size_t i = 0; i < maxThreadsCount; ++i)
	{
		if (i == maxThreadsCount - 1) // ensure to assign all particles
			particlesPerThread = MAX_PARTICLES_COUNT - assignedParticlesCount;

		data.emplace_back(new SimulationData(particlesPerThread, 
			static_cast<uint64_t>(std::chrono::steady_clock::now().time_since_epoch().count())));

		assignedParticlesCount += particlesPerThread;
	}

	activeThreads.store(maxThreadsCount);
}


//-------------------------------------------------------------------------------
ParticleSystem::~ParticleSystem()
{
	stopSimulations.store(true);

	std::chrono::milliseconds sleepTime(10);
	while (activeThreads.load() > 0);
		std::this_thread::sleep_for(sleepTime);
	
	for (size_t i = 0; i < data.size(); ++i)
	{
		delete data[i]->pool;
	}
}


//---------------------------------------------------------------------------
Particle* ParticleSystem::AddEffect(float x, float y, uint8_t count, SimulationData *data)
{
	nvtxRangePush(__FUNCTION__);

	if (count < 1)
		return nullptr;

	Particle *newEffectHead;
	Particle *newEffectTail;

	data->pool->ObtainEffect(count, x, y, data->randomEngine, &newEffectHead, &newEffectTail);
	if (newEffectHead != nullptr)
	{
		newEffectTail->next = data->pAliveParticles;
		data->pAliveParticles = newEffectHead;
	}

	nvtxRangePop();

	return newEffectTail;
}


//---------------------------------------------------------------------------
void ParticleSystem::RequestEffect(float x, float y)
{
	size_t maxThreadsCount = data.size();
	int particlesPerThread = PARTICLES_PER_EFFECT / maxThreadsCount;
	int assignedParticlesCount = 0;
	for (size_t i = 0; i < maxThreadsCount; ++i)
	{
		if (i == data.size() - 1) // ensure to assign all particles
			particlesPerThread = PARTICLES_PER_EFFECT - assignedParticlesCount;

		data[i]->effectRequest.store(EffectRequest(x, y, particlesPerThread));

		assignedParticlesCount += particlesPerThread;
	}
}


//---------------------------------------------------------------------------
void ParticleSystem::Render()
{
	nvtxRangePush(__FUNCTION__);

	for (size_t i = 0; i < data.size(); ++i)
	{
		ParticlesPool *pool = data[i]->pool;
		size_t poolSize = pool->Capacity();
		for (size_t j = 0; j < poolSize; ++j)
		{
			Particle *p = pool->operator[](j);

			const auto atomicData = p->GetAtomicData();

			if (atomicData.lifeTime > 0.0f)
				platform::drawPoint(atomicData.pos.x, atomicData.pos.y, 0.4f, 0.4f, 0.4f, 1.0f);
		}
	}

	nvtxRangePop();
}


//---------------------------------------------------------------------------
void ParticleSystem::Simulate(SimulationData *data)
{
	static const std::uniform_int_distribution<int> distr(1, 100);
	static const std::chrono::milliseconds targetDt(TARGET_SIMULATE_STEP);

	EffectRequest request;

	while (!stopSimulations.load())
	{
		static auto prev = std::chrono::steady_clock::now();
		auto now = std::chrono::steady_clock::now();
		auto delta = std::chrono::duration_cast<std::chrono::milliseconds>(now - prev);
		prev = now;

		request = data->effectRequest.exchange(EffectRequest(0.0f, 0.0f, 0));
		AddEffect(request.x, request.y, request.count, data);

		Particle *runner = data->pAliveParticles;
		Particle *prevRunner = nullptr;
		while (runner != nullptr)
		{
			const auto atomicData = runner->GetAtomicData();
			bool isOffScreen = checkPointOffScreen(atomicData.pos.x, atomicData.pos.y);

			if (atomicData.lifeTime > 0.0f && !isOffScreen)
			{
				runner->Process(atomicData, targetDt.count() / 1000.0f);

				prevRunner = runner;
				runner = runner->next;
			}
			else
			{
				// move particle form alive list back to the pool
				Particle *next = runner->next;
				if (prevRunner == nullptr)
					data->pAliveParticles = next;
				else
					prevRunner->next = next;

				data->pool->ReturnParticle(runner);
				runner = next;

				// respawn effect
				if (!isOffScreen && RESPAWN_PROBABILITY > 0 && distr(data->randomEngine) <= RESPAWN_PROBABILITY)
				{
					Particle *tail = AddEffect(atomicData.pos.x, atomicData.pos.y, PARTICLES_PER_EFFECT, data);
					if (prevRunner == nullptr)
						prevRunner = tail;
				}
			}
		}

		if (delta < targetDt)
			std::this_thread::sleep_for(targetDt - delta);
	}

	activeThreads.fetch_sub(1);
}


//---------------------------------------------------------------------------
void ParticleSystem::RunSimulation()
{
	for (size_t i = 0; i < data.size(); ++i)
	{
		std::thread thread(&ParticleSystem::Simulate, this, data[i]);
		thread.detach();
	}
}