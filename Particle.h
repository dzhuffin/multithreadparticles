#pragma once

#include <random>
#include <atomic>

//-------------------------------------------------------------------------------
struct Vec2
{
	//---------------------------------------------------------------------------
	float x;
	float y;

	//---------------------------------------------------------------------------
	Vec2();
	Vec2(float xValue, float yValue);

	//---------------------------------------------------------------------------
	float GetMagnitude();
	void Normalize();
};


//-------------------------------------------------------------------------------
struct ParticleAtomicData
{
	Vec2 pos;
	float lifeTime;

	ParticleAtomicData() : pos(Vec2()), lifeTime(0.0f) {}
	ParticleAtomicData(float x, float y, float lifeTime) : pos(Vec2(x, y)), lifeTime(lifeTime) {}
};

//-------------------------------------------------------------------------------
class Particle
{
private:

	//---------------------------------------------------------------------------
	std::atomic<ParticleAtomicData> atomicData;
	float speed;
	Vec2 dir;

public:

	//---------------------------------------------------------------------------
	Particle* next;

	//---------------------------------------------------------------------------
	inline void Reset() { atomicData.store(ParticleAtomicData(0.0f, 0.0f, 0.0f)); }
	inline const ParticleAtomicData GetAtomicData() const { return atomicData.load(); }

	//---------------------------------------------------------------------------
	Particle();

	//---------------------------------------------------------------------------
	void Setup(float x, float y, std::mt19937_64& randomEngine);
	void Process(const ParticleAtomicData& atomicData, float dt);
};