#include "Particle.h"
#include "ParticlesPool.h"


//-------------------------------------------------------------------------------
ParticlesPool::ParticlesPool(size_t numParticles) : 
	capacity(numParticles)
{
	pMemory = new Particle[numParticles];
	pHead = pMemory;

	Particle *runner = pMemory;
	for (size_t i = 1; i < numParticles; ++i)
	{
		runner->next = &pMemory[i];
		runner = runner->next;
	}

	runner->next = nullptr;
}


//-------------------------------------------------------------------------------
ParticlesPool::~ParticlesPool()
{
	delete[] pMemory;
}


//-------------------------------------------------------------------------------
void ParticlesPool::ObtainEffect(int particlesCount, float x, float y, std::mt19937_64& randomEngine, Particle **ppHead, Particle **ppTail)
{
	*ppHead = pHead;
	*ppTail = nullptr;

	if (pHead == nullptr)
		return;

	Particle *current = pHead;

	for (int i = 0; i < particlesCount - 1; ++i)
	{
		current->Setup(x, y, randomEngine);
		
		if (current->next == nullptr) // not enough objects in pool
		{
			*ppTail = current;
			pHead = nullptr;
			return;
		}

		current = current->next;
	}

	current->Setup(x, y, randomEngine);
	pHead = current->next;
	current->next = nullptr;

	*ppTail = current;
}


//-------------------------------------------------------------------------------
void ParticlesPool::ReturnParticle(Particle *p)
{
	p->Reset();

	p->next = pHead;
	pHead = p;
}