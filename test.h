#include <stdint.h>

#define _ENABLE_ATOMIC_ALIGNMENT_FIX   // in order to compile with MSVC 2015 Update 2

namespace test
{
	const float SCREEN_WIDTH = 1024;
	const float SCREEN_HEIGHT = 768;

	const int TARGET_SIMULATE_STEP = 16; // in milliseconds

	const float GRAVITY_Y = 30.0f;
	const float PARTICLE_DECERLERATION = 50.0f;
	const float PARTICLE_LIFETIME_MIN = 0.5f; // in seconds
	const float PARTICLE_LIFETIME_MAX = 5.0f; // in seconds
	const float PARTICLE_SPEED_X_MIN = 50.0f;
	const float PARTICLE_SPEED_X_MAX = 250.0f;
	const float PARTICLE_SPEED_Y_MIN = 20.0f;
	const float PARTICLE_SPEED_Y_MAX = 120.0f;

	const int RESPAWN_PROBABILITY = 2; // in percents
	const uint8_t PARTICLES_PER_EFFECT = 64;
	const int MAX_PARTICLES_COUNT = 2048 * PARTICLES_PER_EFFECT;


	void render(void); // Only platform::drawPoint should be used
	void update(int dt); // dt in milliseconds
	void on_click(int x, int y); // x, y - in pixels

	void init(void);
	void term(void);

	bool checkPointOffScreen(float x, float y);
};

namespace platform
{
	void drawPoint(float x, float y, float r, float g, float b, float a);
};