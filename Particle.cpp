#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "test.h"
#include "Particle.h"

//-------------------------------------------------------------------------------
using namespace test;

//-------------------------------------------------------------------------------
Vec2::Vec2() : x(0.0f), y(0.0f) {}
Vec2::Vec2(float xValue, float yValue) : x(xValue), y(yValue) {}


//-------------------------------------------------------------------------------
float Vec2::GetMagnitude()
{
	return sqrt((x * x) + (y * y));
}


//-------------------------------------------------------------------------------
void Vec2::Normalize()
{
	float magnitude = GetMagnitude();
	x = x / magnitude;
	y = y / magnitude;
}


//-------------------------------------------------------------------------------
Particle::Particle() :
	atomicData(ParticleAtomicData(0.0f, 0.0f, 0.0f)), dir(Vec2()), speed(0.0f), next(nullptr)
{
}


//-------------------------------------------------------------------------------
void Particle::Setup(float x, float y, std::mt19937_64& randomEngine)
{
	static const std::uniform_real_distribution<float> distr(0.0f, 1.0f);

	atomicData.store(ParticleAtomicData(x, y, distr(randomEngine) * 
		(PARTICLE_LIFETIME_MAX - PARTICLE_LIFETIME_MIN) + PARTICLE_LIFETIME_MIN));

	float xSign = (distr(randomEngine) > 0.5f) ? -1.0f : 1.0f;
	float ySign = (distr(randomEngine) > 0.5f) ? -1.0f : 1.0f;

	dir.x = xSign * (distr(randomEngine) * (PARTICLE_SPEED_X_MAX - PARTICLE_SPEED_X_MIN) + PARTICLE_SPEED_X_MIN);
	dir.y = ySign * (distr(randomEngine) * (PARTICLE_SPEED_Y_MAX - PARTICLE_SPEED_Y_MIN) + PARTICLE_SPEED_Y_MIN);

	speed = dir.GetMagnitude();
	dir.Normalize();
}


//-------------------------------------------------------------------------------
void Particle::Process(const ParticleAtomicData& oldAtomicData, float dt)
{
	atomicData.store(ParticleAtomicData(
		oldAtomicData.pos.x + dir.x * speed * dt, 
		oldAtomicData.pos.y + (dir.y * speed - GRAVITY_Y) * dt, 
		oldAtomicData.lifeTime - dt));

	speed -= PARTICLE_DECERLERATION * dt;
	if (speed < 0.0f)
		speed = 0.0f;
}