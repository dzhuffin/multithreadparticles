#pragma once

#include <vector>
#include <random>

class ParticlesPool;
class Particle;

//-------------------------------------------------------------------------------
class ParticleSystem
{
private:

	//---------------------------------------------------------------------------
	struct EffectRequest
	{
		float x;
		float y;
		uint8_t count;

		EffectRequest() {}
		EffectRequest(float rx, float ry, uint8_t c) : x(rx), y(ry), count(c) {}
	};

	//---------------------------------------------------------------------------
	struct SimulationData
	{
		ParticlesPool *pool;
		Particle* pAliveParticles;
		std::atomic<EffectRequest> effectRequest;
		std::mt19937_64 randomEngine;

		SimulationData(size_t numParticles, uint64_t randomSeed);
		~SimulationData();
	};

	//---------------------------------------------------------------------------
	std::vector<SimulationData*> data;

	//---------------------------------------------------------------------------
	std::atomic<bool> stopSimulations;
	std::atomic<size_t> activeThreads;

	// returns pointer to created effect tail
	Particle* AddEffect(float x, float y, uint8_t count, SimulationData *data);

public:

	//---------------------------------------------------------------------------
	ParticleSystem();
	virtual ~ParticleSystem();

	//---------------------------------------------------------------------------
	void Render();
	void Simulate(SimulationData *data);

	//---------------------------------------------------------------------------
	void RunSimulation();

	//---------------------------------------------------------------------------
	void RequestEffect(float x, float y);
};

