#pragma once

//-------------------------------------------------------------------------------
class ParticlesPool
{
private:

	//---------------------------------------------------------------------------
	size_t capacity;

	//---------------------------------------------------------------------------
	Particle *pMemory;
	Particle *pHead;

public:

	//---------------------------------------------------------------------------
	ParticlesPool(size_t numParticles);
	virtual ~ParticlesPool();

	//---------------------------------------------------------------------------
	void ObtainEffect(int particlesCount, float x, float y, std::mt19937_64& randomEngine, Particle **ppHead, Particle **ppTail);
	void ReturnParticle(Particle *p);

	//---------------------------------------------------------------------------
	inline size_t Capacity() const { return capacity; }
	Particle *operator[](int i) { return pMemory + i; }
};

