#include <thread>
#include <mutex>
#include <atomic>

#include "./nvToolsExt.h"

#include "test.h"
#include "ParticleSystem.h"


//-------------------------------------------------------------------------------
ParticleSystem p;


//-------------------------------------------------------------------------------
bool test::checkPointOffScreen(float x, float y)
{
	return x < 0.0f || x > test::SCREEN_WIDTH ||
		   y < 0.0f || y > test::SCREEN_HEIGHT;
}


//-------------------------------------------------------------------------------
void test::init(void)
{
	p.RunSimulation();
}


//-------------------------------------------------------------------------------
void test::term(void)
{
}


//-------------------------------------------------------------------------------
void test::render(void)
{
	p.Render();
}


//-------------------------------------------------------------------------------
void test::update(int dt)
{
}


//-------------------------------------------------------------------------------
void test::on_click(int x, int y)
{
	p.RequestEffect(static_cast<float>(x), static_cast<float>(SCREEN_HEIGHT - y));
}